import axios from 'axios';
import { PostJSON } from 'models/Post';

export async function fetch<Type>(path: string): Promise<Type> {
  try {
    const response = await axios.get(process.env.API_URL + path);
    return response.data;
  } catch (err) {
    throw new Error(`Fetching data from "${path}" failed!`);
  }
}

export async function fetchPosts(): Promise<PostJSON[]> {
  return await fetch<PostJSON[]>('/posts');
}

export async function fetchPost(id: number): Promise<PostJSON> {
  return await fetch<PostJSON>(`/posts/${id}`);
}
