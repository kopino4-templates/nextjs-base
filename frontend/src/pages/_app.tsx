import { NavBar } from 'components/common/NavBar';
import type { AppProps } from 'next/app';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <main>
        <NavBar />
        <Component {...pageProps} />
      </main>
    </>
  );
}

export default MyApp;
