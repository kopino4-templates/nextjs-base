import { GetServerSideProps } from 'next';
import { fetchPost } from 'utils/helpers';
import { Post, PostJSON } from 'models/Post';
import { ParsedUrlQuery } from 'querystring';
import { PostSSR } from 'components/pages/posts/ssr/PostSSR';

type Props = {
  postJson: PostJSON;
};

export default function PostSSRPage({ postJson }: Props) {
  const post = new Post(postJson);
  return <PostSSR post={post} />;
}

interface Params extends ParsedUrlQuery {
  id: string;
}

export const getServerSideProps: GetServerSideProps<Props, Params> = async (context) => {
  try {
    const params = context.params!;
    const id = parseInt(params.id);
    const postJson = await fetchPost(id);
    return {
      props: {
        postJson,
      },
    };
  } catch (e) {
    return { notFound: true };
  }
};
