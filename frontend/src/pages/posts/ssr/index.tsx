import { GetServerSideProps } from 'next';
import { fetchPosts } from 'utils/helpers';
import { Post, PostJSON } from 'models/Post';
import { PostsSSR } from 'components/pages/posts/ssr/PostsSSR';

type Props = {
  postsJson: PostJSON[];
};

export default function PostsSSRPage({ postsJson }: Props) {
  const posts = Post.initList(postsJson);
  return <PostsSSR posts={posts} />;
}

export const getServerSideProps: GetServerSideProps<Props> = async (_) => {
  const postsJson = await fetchPosts();
  return {
    props: {
      postsJson,
    },
  };
};
