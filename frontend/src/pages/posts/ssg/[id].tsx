import { GetStaticProps, GetStaticPaths } from 'next';
import { fetchPosts, fetchPost } from 'utils/helpers';
import { Post, PostJSON } from 'models/Post';
import { ParsedUrlQuery } from 'querystring';
import { PostSSG } from 'components/pages/posts/ssg/PostSSG';

type Props = {
  postJson: PostJSON;
};

export default function PostSSGPage({ postJson }: Props) {
  const post = new Post(postJson);
  return <PostSSG post={post} />;
}

interface Params extends ParsedUrlQuery {
  id: string;
}

export const getStaticProps: GetStaticProps<Props, Params> = async (context) => {
  const params = context.params!;
  const id = parseInt(params.id);
  const postJson = await fetchPost(id);
  return {
    props: {
      postJson,
    },
  };
};

export const getStaticPaths: GetStaticPaths<Params> = async (_) => {
  const postsJson = await fetchPosts();

  const paths = postsJson.map((post) => {
    return {
      params: {
        id: `${post.id}`,
      },
    };
  });

  return {
    paths: paths,
    fallback: false,
  };
};
