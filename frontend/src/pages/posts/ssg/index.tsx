import { GetStaticProps } from 'next';
import { fetchPosts } from 'utils/helpers';
import { Post, PostJSON } from 'models/Post';
import { PostsSSG } from 'components/pages/posts/ssg/PostsSSG';

type Props = {
  postsJson: PostJSON[];
};

export default function PostsSSGPage({ postsJson }: Props) {
  const posts = Post.initList(postsJson);
  return <PostsSSG posts={posts} />;
}

export const getStaticProps: GetStaticProps<Props> = async (_) => {
  const postsJson = await fetchPosts();
  return {
    props: {
      postsJson,
    },
  };
};
