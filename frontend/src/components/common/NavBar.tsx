import Link from 'next/link';
import Image from 'next/image';
import logo from '@/public/logo.svg';

export function NavBar() {
  const logoHeight = 46;
  const widthHeightRatio = 2.73;

  return (
    <>
      <Link href="/">
        <Image src={logo} priority alt="logo" height={logoHeight} width={logoHeight * widthHeightRatio} />
      </Link>
      <h3>
        <Link href="/about" legacyBehavior>
          About page
        </Link>
      </h3>
      <h3>
        <Link href="/posts/ssg" legacyBehavior>
          Posts page (SSG)
        </Link>
      </h3>
      <h3>
        <Link href="/posts/ssr" legacyBehavior>
          Posts page (SSR)
        </Link>
      </h3>
    </>
  );
}
