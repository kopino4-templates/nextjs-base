import { SeoBaseline } from 'components/common/SeoBaseline';
import Image from 'next/image';
import photo from '@/public/photo.jpg';

export function Home() {
  return (
    <>
      <SeoBaseline
        title="Home | Title giving user insight into content (max 60 characters)"
        description="This text should summarise the topic on this page in max 160 characters"
      />

      <h1>Welcome to the Home Page!</h1>

      <Image priority src={photo} alt="photo" layout="responsive" />

      <h2>Every page should have a moderate amount of content to rank higher</h2>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
        magna aliqua. Tempus urna et pharetra pharetra massa massa ultricies. Amet nulla facilisi morbi tempus iaculis
        urna id. Tincidunt lobortis feugiat vivamus at augue. Mauris rhoncus aenean vel elit scelerisque mauris
        pellentesque pulvinar. Velit egestas dui id ornare arcu odio ut sem. Posuere sollicitudin aliquam ultrices
        sagittis orci a scelerisque purus semper. Faucibus scelerisque eleifend donec pretium vulputate sapien nec
        sagittis aliquam. Augue ut lectus arcu bibendum at varius vel pharetra. Diam donec adipiscing tristique risus
        nec feugiat in fermentum posuere.
      </p>

      <p>
        Ipsum a arcu cursus vitae congue mauris rhoncus aenean. Tincidunt augue interdum velit euismod in pellentesque.
        Scelerisque mauris pellentesque pulvinar pellentesque. Pharetra diam sit amet nisl suscipit adipiscing bibendum
        est ultricies. Amet dictum sit amet justo. Et malesuada fames ac turpis egestas integer. Velit dignissim sodales
        ut eu sem. Dui id ornare arcu odio ut. At imperdiet dui accumsan sit amet nulla facilisi morbi tempus. Faucibus
        interdum posuere lorem ipsum. Cursus vitae congue mauris rhoncus aenean vel elit scelerisque.
      </p>

      <p>
        Dictum non consectetur a erat nam at. Vulputate ut pharetra sit amet aliquam. Nibh sed pulvinar proin gravida
        hendrerit lectus. Semper auctor neque vitae tempus quam pellentesque nec nam aliquam. Risus quis varius quam
        quisque id. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida. Placerat orci nulla
        pellentesque dignissim enim sit amet venenatis urna. Nec feugiat in fermentum posuere urna. Vitae tortor
        condimentum lacinia quis vel eros donec ac odio. Dictum sit amet justo donec enim diam vulputate ut. Eu augue ut
        lectus arcu bibendum. Massa massa ultricies mi quis hendrerit dolor.
      </p>

      <p>
        Nibh venenatis cras sed felis. Vulputate mi sit amet mauris commodo. Sed vulputate mi sit amet mauris commodo
        quis imperdiet massa. Mi ipsum faucibus vitae aliquet nec ullamcorper sit amet. Enim ut sem viverra aliquet.
        Sodales neque sodales ut etiam. Orci phasellus egestas tellus rutrum tellus pellentesque eu. Purus gravida quis
        blandit turpis cursus. Quis risus sed vulputate odio ut enim. Ut ornare lectus sit amet est placerat. Tellus
        integer feugiat scelerisque varius morbi enim nunc faucibus. Semper eget duis at tellus. Sed vulputate odio ut
        enim blandit. Sagittis eu volutpat odio facilisis mauris sit. Risus viverra adipiscing at in tellus integer.
        Malesuada fames ac turpis egestas integer eget aliquet.
      </p>

      <p>
        Arcu non odio euismod lacinia at quis risus sed vulputate. Integer eget aliquet nibh praesent tristique. Vitae
        et leo duis ut diam quam nulla porttitor massa. Justo donec enim diam vulputate ut pharetra. Donec ultrices
        tincidunt arcu non sodales neque. Et tortor consequat id porta nibh venenatis cras. Faucibus pulvinar elementum
        integer enim neque volutpat ac tincidunt. Porttitor massa id neque aliquam vestibulum morbi. Volutpat odio
        facilisis mauris sit amet massa vitae. Varius morbi enim nunc faucibus a pellentesque sit. Hac habitasse platea
        dictumst vestibulum. In massa tempor nec feugiat nisl. Consectetur adipiscing elit ut aliquam. Mauris in aliquam
        sem fringilla. Augue eget arcu dictum varius duis at. Vel risus commodo viverra maecenas accumsan lacus vel.
        Vulputate dignissim suspendisse in est ante in nibh mauris cursus. Dolor sit amet consectetur adipiscing elit
        pellentesque habitant morbi. Pharetra massa massa ultricies mi. A pellentesque sit amet porttitor eget dolor
        morbi non.
      </p>
    </>
  );
}
