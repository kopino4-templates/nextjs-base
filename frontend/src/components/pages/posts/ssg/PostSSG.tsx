import { Post } from 'models/Post';
import { SeoBaseline } from 'components/common/SeoBaseline';

type Props = {
  post: Post;
};

export function PostSSG({ post }: Props) {
  return (
    <>
      <SeoBaseline
        title="Post SSG | Title giving user insight"
        description="Server-side generated post site which fetches the data at build time"
      />

      <h1>Post {post.id} (SSG)</h1>
      <div key={post.id}>
        <h2>{post.title}</h2>
        <p>{post.body}</p>
      </div>
    </>
  );
}
