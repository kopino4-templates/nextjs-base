import Link from 'next/link';
import { Post } from 'models/Post';
import { SeoBaseline } from 'components/common/SeoBaseline';

type Props = {
  posts: Post[];
};

export function PostsSSG({ posts }: Props) {
  return (
    <>
      <SeoBaseline
        title="Posts SSG | Title giving user insight"
        description="Server-side generated posts site which fetches the data at build time"
      />

      <h1>Posts Page (SSG)</h1>
      {posts.map((post) => (
        <div key={post.id}>
          <h2>
            <Link href={`/posts/ssg/${post.id}`}>{post.title}</Link>
          </h2>
          <p>{post.body}</p>
        </div>
      ))}
    </>
  );
}
