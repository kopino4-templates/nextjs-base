import { Post } from 'models/Post';
import { SeoBaseline } from 'components/common/SeoBaseline';

type Props = {
  post: Post;
};

export function PostSSR({ post }: Props) {
  return (
    <>
      <SeoBaseline
        title="Post SSR | Title giving user insight"
        description="Server-side rendered post site which fetches the data at build time"
      />

      <h1>Post {post.id} (SSR)</h1>
      <div key={post.id}>
        <h2>{post.title}</h2>
        <p>{post.body}</p>
      </div>
    </>
  );
}
