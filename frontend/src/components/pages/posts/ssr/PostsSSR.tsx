import Link from 'next/link';
import { Post } from 'models/Post';
import { SeoBaseline } from 'components/common/SeoBaseline';

type Props = {
  posts: Post[];
};

export function PostsSSR({ posts }: Props) {
  return (
    <>
      <SeoBaseline
        title="Posts SSR | Title giving user insight"
        description="Server-side rendered posts site which fetches the data at request time on the server and returns the populated HTML"
      />

      <h1>Posts Page (SSR)</h1>
      {posts.map((post) => (
        <div key={post.id}>
          <h2>
            <Link href={`/posts/ssr/${post.id}`}>{post.title}</Link>
          </h2>
          <p>{post.body}</p>
        </div>
      ))}
    </>
  );
}
