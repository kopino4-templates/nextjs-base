#!/bin/bash

##
## Run docker-compose with DEVELOPMENT configuration
##

SCRIPT_PATH=$(realpath "$BASH_SOURCE")
PARENT_DIR=$(dirname "$SCRIPT_PATH")
ROOT=$(dirname "$PARENT_DIR")

echo "Changing to project's root directory: $ROOT"
cd "$ROOT"

echo "Running docker-compose up --build in DEVELOPMENT mode"
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up --build
