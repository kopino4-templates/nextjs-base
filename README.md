# Dockerized Nextjs application

Containerized feature rich and customizable Next.js application

## Intro

So you just decided to start a Next.js project which is essentially a
framework for React.

The question is: What should you do next?

Should you create one from scratch? Or is it better to customize
an already existing template to your needs?

I'm going to show you how to use this template for your project, it is
then up to you to implement the Use cases you need.

## Feature overview

*   [x] **Server-side rendering**
*   [x] **Static site generation**
*   [x] **Fetching of data**
*   [x] **Fetching of data**
*   [x] **Hot reloading** in development mode
*   [ ] **Tests**

## Contents

*   [What is this?](#what-is-this)
*   [When should I use this?](#when-should-i-use-this)
*   [Getting started](#getting-started)
    *   [Requirements](#requirements)
    *   [Install](#install)
    *   [Configuration](#configuration)
    *   [Usage](#usage)
*   [About](#about)
    * [Used Technologies](#used-technologies)
    * [Testing](#testing)
    * [Logging](#logging)
*   [Contribute](#contribute)
*   [License](#license)
*   [Sources](#sources)
*   [Conclusion](#conclusion)

## What is this?

This project is an exhaustive Next.js template that you can customize to your needs.
You can learn by example on how to implement the core functionalities.

## Why should I use this?

There are many templates but this one is a feature rich template where you can choose
the features you need just by choosing the right version of this project.

It is fully containerized with docker, hence very easy do deploy in any docker environment.

## Getting Started

So how do you get this template to work for your project? It is easier than you think.

### Requirements

* Basic knowledge of docker
* Basic knowledge of docker-compose
* That&apos;s it

### Install

Use git to clone this repository into your computer.

```
git clone https://gitlab.com/kopino4-templates/nextjs-base
```

### Configuration

To configure the application create a `.env` file following the `.env.example` structure

### Usage

Run in development mode

```bash
./scripts/dev_run.sh
```

Run in production mode

```bash
./scripts/prod_run.sh
```

Hot reload works during development by mounting volumes to the **./frontend/src** folder

## About

Let&apos; dive deeper into the template

### Used technologies

Ve are using Next.js, which is an amazing React framework. The feature highlights are, that
you can choose between Server Side rendering or Static Site Generation to boost performance.

### Testing

No test are implemented yet, but are comming soon.

### Logging

Loggin is an essential part to debug your application. That is why it is part of this template.

## Contribute

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Sources

[Next.js][nextjs] - The leading React framework for production

[//]: # "Source definitions"
[nextjs]: https://nextjs.org/ "Next.js"

## Conclusion

To summarize..

We have an exhaustive Next.js template with many features.
In our future projects we can use this template to get a great head start in creating a custom Next.js project.

